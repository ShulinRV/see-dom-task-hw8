const redColor = "#ff0000";

const allParagraph = document.querySelectorAll("p");

for (let elem of allParagraph) {
  if ((elem.style.color = "black")) {
    elem.style.color = redColor;
  }
}

const opList = document.querySelector("#optionsList");
console.log(opList);

for (let key of opList.children) {
  console.log(key.nodeType, key.nodeName);
}

// const parentElem = opList.closest("#optionsList");
// console.log(parentElem);

const childElem = opList.querySelector("#optionsList");
console.log(childElem);

const testP = document.querySelector("#testParagraph");
testP.textContent = "This is a paragraph";
console.log(testP);

const mainHeader = document.querySelectorAll(".main-header > *");
console.log(mainHeader);

for (let elem of mainHeader) {
  elem.classList.add("nav-item");
}
console.log(mainHeader);

const titleElems = document.querySelectorAll(".section-title");
console.log(titleElems);

for (let elem of titleElems) {
  elem.classList.remove("section-title");
}
console.log(titleElems);
